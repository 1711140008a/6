class Tweet < ApplicationRecord
  validates :message, presence: true, length: {maximum: 140}
  
  belongs_to :user
  has_many :likes, dependent: :destroy
  has_many :like_users, source: :user, through: :likes
  
  def like(user)
    likes.create(user_id: user.id)
  end
  def unlike(user)
    likes.find_by(user_id: user.id).destroy
  end
  def liked?(user)
    pp like_users
    like_users.include?(user)
  end
end

